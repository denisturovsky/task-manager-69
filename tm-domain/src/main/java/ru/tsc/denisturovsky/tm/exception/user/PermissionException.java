package ru.tsc.denisturovsky.tm.exception.user;

public class PermissionException extends AbstractUserException {

    public PermissionException() {
        super("Error! You do not have permission.");
    }

}
