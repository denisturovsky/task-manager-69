package ru.tsc.denisturovsky.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}
