package ru.tsc.denisturovsky.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.denisturovsky.tm.configuration.ServerConfiguration;

@UtilityClass
public final class ContextTestData {

    @NotNull
    public final static ApplicationContext CONTEXT = new AnnotationConfigApplicationContext(ServerConfiguration.class);

}
