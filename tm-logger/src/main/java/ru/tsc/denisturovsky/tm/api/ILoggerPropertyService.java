package ru.tsc.denisturovsky.tm.api;

import org.jetbrains.annotations.NotNull;

public interface ILoggerPropertyService {

    @NotNull
    String getJMSServerHost();

    @NotNull
    String getJMSServerPort();

    @NotNull
    String getMDBServerHost();

    @NotNull
    String getMDBServerPort();

}
